CREATE TABLE Salesman(Salesman_id integer, Name Varchar(20), City varchar(20), Commission double(3,2));
INSERT INTO Salesman VALUES(5001,"James Hoog","New York",0.15);
INSERT INTO Salesman VALUES(5002,"Nail Knite","Paris",0.13);
INSERT INTO Salesman VALUES(5005,"Pit Alex","London",0.11);
INSERT INTO Salesman VALUES(5006,"Mc Lyon","Paris",0.14);
INSERT INTO Salesman VALUES(5003,"Lauson Hen","Sydney",0.12);
INSERT INTO Salesman VALUES(5007,"Paul Adam","Rome",0.13);